# -*- coding: utf-8 -*-
from django.db import models


class Customer(models.Model):
    id = models.CharField(primary_key=True, max_length=20,
                          db_column='CustomerID')
    company_name = models.CharField(u'Nome da Empresa', max_length=40,
                                    db_column='CompanyName')
    contact_name = models.CharField(u'Nome do contato', max_length=30,
                                    db_column='ContactName')
    contact_title = models.CharField(u'Cargo do contato', max_length=30,
                                     db_column='ContactTitle')
    address = models.CharField(u'Endereço', max_length=60, db_column='Address')
    city = models.CharField(u'Cidade', max_length=15, db_column='City')
    region = models.CharField(u'Região', max_length=15, db_column='Region')
    postal_code = models.CharField(u'CEP', max_length=10,
                                   db_column='PostalCode')
    country = models.CharField(u'Cidade', max_length=15, db_column='Country')
    phone = models.CharField(u'Telefone', max_length=24, db_column='Phone')
    fax = models.CharField(u'Fax', max_length=24, db_column='Fax')


    class Meta:
        app_label = 'galeygo_admin'
        db_table = 'Customers'
        verbose_name = "Cliente"
