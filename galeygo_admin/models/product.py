# -*- coding: utf-8 -*-
from django.db import models


class Product(models.Model):
    id = models.IntegerField(primary_key=True, db_column='ProductID')
    product_name = models.CharField(u'Nome do produto', max_length=40,
                                    db_column='ProductName')
    supplier_id = models.IntegerField(u'Id do fornecedor',
                                      db_column='SupplierId')
    category_id = models.IntegerField(u'Id da Categoria',
                                      db_column='CategoryId')
    quantity_per_unity = models.CharField(u'Quantidade por unidade',
                                          max_length=20,
                                          db_column='QuantityPerUnit')
    unit_price = models.DecimalField(u'Preço unitário', max_digits=11,
                                     decimal_places=2, db_column='UnitPrice')
    units_in_stock = models.SmallIntegerField(u'Unidades em estoque',
                                              db_column='UnitsInStock')
    units_on_order = models.SmallIntegerField(u'Unidades em pedidos',
                                              db_column='UnitsOnOrder')
    reorder_level = models.SmallIntegerField(u'Nível de pedido',
                                             db_column='ReorderLevel')
    discontinued = models.BooleanField(u'Produto descontinuado',
                                       db_column='Discontinued')


    class Meta:
        app_label = 'galeygo_admin'
        db_table = 'Products'
        verbose_name = "Produto"
