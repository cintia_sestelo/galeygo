# -*- coding: utf-8 -*-
from django.db import models


class Region(models.Model):
    id = models.IntegerField(primary_key=True, db_column='RegionID')
    region_description = models.CharField(u'Descrição da região', max_length=50,
                                          db_column='RegionDescription')

    class Meta:
        app_label = 'galeygo_admin'
        db_table = 'Region'
        verbose_name = "Região"
        verbose_name_plural = "Regiões"
