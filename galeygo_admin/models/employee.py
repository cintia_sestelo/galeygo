# -*- coding: utf-8 -*-
from django.db import models


class Employee(models.Model):
    id = models.IntegerField(primary_key=True, db_column='EmployeeID')
    last_name = models.CharField(u'Sobrenome', max_length=20,
                                    db_column='LastName')
    first_name = models.CharField(u'Nome', max_length=10, db_column='FirstName')
    title = models.CharField(u'Cargo', max_length=30, db_column='Title')
    title_of_courtesy = models.CharField(u'Cargo de cortesia', max_length=25,
                                         db_column='TitleOfCourtesy')
    birth_date = models.DateField(u'Data de nascimento', db_column='BirthDate')
    hire_date = models.DateField(u'Data de contratação', db_column='HireDate')
    address = models.CharField(u'Endereço', max_length=60, db_column='Address')
    city = models.CharField(u'Cidade', max_length=15, db_column='City')
    region = models.CharField(u'Região', max_length=15, db_column='Region')
    postal_code = models.CharField(u'CEP', max_length=10,
                                   db_column='PostalCode')
    country = models.CharField(u'País', max_length=15, db_column='Country')
    home_phone = models.CharField(u'Telefone Residencial', max_length=24,
                                  db_column='HomePhone')
    extension = models.CharField(u'Extensão', max_length=4,
                                 db_column='Extension')
    photo = models.ImageField(u'Foto', db_column='Photo')
    notes = models.TextField(u'Notas', db_column='Notes')
    reports_to = models.IntegerField(u'Superior', db_column='ReportsTo')
    photo_path = models.CharField(u'Link da Foto', max_length=255,
                                  db_column='PhotoPath')


    class Meta:
        app_label = 'galeygo_admin'
        db_table = 'Employees'
        verbose_name = u"Funcionário"
