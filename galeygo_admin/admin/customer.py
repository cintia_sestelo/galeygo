# -*- coding: utf-8 -*-
from django.contrib import admin

from galeygo_admin.models import Customer


class CustomerAdmin(admin.ModelAdmin):
    model = Customer
    list_display = ('id', 'company_name', 'contact_name', 'contact_title')

    list_filter = ('region', )
