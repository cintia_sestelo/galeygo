# -*- coding: utf-8 -*-
from django.contrib import admin

from galeygo_admin.models import Product


class ProductAdmin(admin.ModelAdmin):
    model = Product
    list_display = ('id', 'product_name', 'unit_price', 'units_in_stock',
                    'units_on_order')

    actions = (u'change_unit_price', )

    def change_unit_price(self, request, queryset):
        queryset.update(unit_price=1)
        

    change_unit_price.short_description = u'Modificar valor unitário'
