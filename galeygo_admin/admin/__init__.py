# -*- coding: utf-8 -*-
from django.contrib import admin

from galeygo_admin.admin.region import RegionAdmin
from galeygo_admin.admin.product import ProductAdmin
from galeygo_admin.admin.customer import CustomerAdmin
from galeygo_admin.admin.employee import EmployeeAdmin
from galeygo_admin.models import Region, Product, Customer, Employee


admin.site.register(Region, RegionAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(Customer, CustomerAdmin)
admin.site.register(Employee, EmployeeAdmin)
