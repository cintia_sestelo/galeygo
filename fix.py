import csv

from galeygo_admin.models import Customer


def migrate():
    for id_, bad_name in csv.reader(open('badcustomers.csv'), delimiter=';'):
        new_name = bad_name.decode('iso-8859-1').encode('utf-8')
        Customer.objects.filter(id=id_).update(address=new_name)

if __name__ == '__main__':
    migrate()
