# -*- coding: utf-8 -*-

import unittest
from challenge4 import palindrome


class TestPalindrome(unittest.TestCase):
    def test_is_palindrome(self):
        word = u'ama'
        self.assertTrue(palindrome(word))

    def test_not_a_palindrome(self):
        word = u'amar'
        self.assertFalse(palindrome(word))

    def test_with_empty_string(self):
        word = u''
        self.assertRaises(ValueError, palindrome, word)

    def test_with_punctuation_only(self):
        word = u',. ;;?'
        self.assertRaises(ValueError, palindrome, word)

    def test_with_upper_case(self):
        word = u'Radar'
        self.assertTrue(palindrome(word))

    def test_with_blank_space(self):
        word = u'Socorram-me, subi no ônibus em Marrocos'
        self.assertTrue(palindrome(word))

    def test_string_with_accentuation(self):
        word = u'somávamos'
        self.assertTrue(palindrome(word))


unittest.main()
