import unittest
from challenge3 import list_generator, list_len


class TestListGenerator(unittest.TestCase):
    def test_list_len_with_elements(self):
        l = [2, 'cachorro-quente', 'hamburguer', 35]
        self.assertEqual(list_len(l), 4)

    def test_list_len_with_empty_list(self):
        l = []
        self.assertEqual(list_len(l), 0)

    def test_two_same_size_lists(self):
        list1 = [3, 5, 6]
        list2 = ['brigadeiros', 'beijinhos', 'cajuzinhos']
        self.assertEqual(list_generator(list1, list2),
                        [3, 'brigadeiros', 5, 'beijinhos', 6, 'cajuzinhos'])

    def test_with_two_empty_lists(self):
        list1 = []
        list2 = []
        self.assertEqual(list_generator(list1, list2), [])

    def test_with_one_empty_list(self):
        list1 = []
        list2 = ['chamito', 'yakult']
        self.assertRaises(ValueError, list_generator, list1, list2)

    def test_with_different_size_lists(self):
        list1 = [1]
        list2 = ['chamito', 'yakult']
        self.assertRaises(ValueError, list_generator, list1, list2)


unittest.main()
