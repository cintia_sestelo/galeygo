# -*- coding: utf-8 -*-

inverted = {
    u'a': [u'á', u'à', u'ã', u'â', u'A', u'ä', u'Á', u'À', u'Ã', u'Â', u'Ä'],
    u'e': [u'é', u'è', u'ẽ', u'ê', u'E', u'ë', u'É', u'È', u'Ẽ', u'Ê', u'Ë'],
    u'i': [u'í', u'ì', u'ĩ', u'î', u'I', u'ï', u'Í', u'Ì', u'Ĩ', u'Î', u'Ï'],
    u'o': [u'ó', u'ò', u'õ', u'ô', u'O', u'ö', u'Ó', u'Ò', u'Õ', u'Ô', u'Ö'],
    u'u': [u'ú', u'ù', u'ũ', u'û', u'U', u'ü', u'Ú', u'Ù', u'Ũ', u'Û', u'Ü'],
    u'b': [u'B'],
    u'c': [u'C'],
    u'd': [u'D'],
    u'f': [u'F'],
    u'g': [u'G'],
    u'h': [u'H'],
    u'j': [u'J'],
    u'l': [u'L'],
    u'm': [u'M'],
    u'n': [u'N'],
    u'p': [u'P'],
    u'q': [u'Q'],
    u'r': [u'R'],
    u's': [u'S'],
    u't': [u'T'],
    u'v': [u'V'],
    u'x': [u'X'],
    u'z': [u'Z'],
    u'w': [u'W'],
    u'y': [u'Y'],
    u'k': [u'K'],
}

substitutions = {}
for key, values in inverted.items():
    for value in values:
        substitutions[value] = key
    substitutions[key] = key
