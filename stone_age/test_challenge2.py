import unittest
from challenge2 import sum_multiples


class TestSumMultiples(unittest.TestCase):
    def test_with_natural(self):
        natural = 15
        self.assertEqual(sum_multiples(natural), 45)

    def test_with_zero(self):
        natural = 0
        self.assertEqual(sum_multiples(natural), 0)

    def test_with_negative_number(self):
        natural = -15
        self.assertEqual(sum_multiples(natural), -45)

    def test_with_higher_natural(self):
        natural = 99
        self.assertEqual(sum_multiples(natural), 1683)

    def test_with_low_natural(self):
        natural = 2
        self.assertEqual(sum_multiples(natural), 0)

    def test_with_minimum_natural(self):
        natural = 3
        self.assertEqual(sum_multiples(natural), 3)

    def test_with_high_natural(self):
        natural = 1000
        self.assertEqual(sum_multiples(natural), 166833)


unittest.main()
