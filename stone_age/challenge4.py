from normalization_table import substitutions


def normalization(raw_word):
    pos_normalization = []
    for letter in raw_word:
        if letter in substitutions:
            pos_normalization.append(substitutions[letter])
    return pos_normalization


def palindrome(word):
        word = normalization(word)
        if not word:
            raise ValueError('Not a valid string.')
        else:
            return word == word[::-1]
