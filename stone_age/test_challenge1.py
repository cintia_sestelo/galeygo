import unittest
from challenge1 import max_value


class TestMaxValue(unittest.TestCase):
    def test_with_natural_numbers(self):
        sample = [3, 86, 6, 17, 3, 8, 34, 0]
        self.assertEqual(max_value(sample), 86)

    def test_with_empty_list(self):
        sample = []
        self.assertIsNone(max_value(sample))

    def test_with_negative_numbers(self):
        sample = [-7, 10, 15, -89, 103, 55, -91]
        self.assertEqual(max_value(sample), 103)

    def test_with_duplicated_numbers(self):
        sample = [-7, 103, 215, -89, 103, 103, 55, -91, -7]
        self.assertEqual(max_value(sample), 215)

    def test_only_with_negative_numbers(self):
        sample = [-7, -103, -215, -89, -103, -103, -55, -91, -7]
        self.assertEqual(max_value(sample), -7)

    def test_list_with_one_element(self):
        sample = [-2]
        self.assertEqual(max_value(sample), -2)


unittest.main()
