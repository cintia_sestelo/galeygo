def sum_multiples(natural):
    #allowing receive negative numbers
    if natural < 0:
        numbers = xrange(natural, 2)
    else:
        numbers = xrange(1, natural + 1)

    final_number = 0
    for number in numbers:
        if number % 3 == 0:
            final_number += number
    return final_number
