def largest_palindrome():
    palindrome = 0
    for number in xrange(999, 100, -1):
        for e_number in xrange(999, 100, -1):
            product = number * e_number
            if str(product) == str(product)[::-1]:
                if product > palindrome:
                    palindrome = product
    return palindrome


print largest_palindrome()
