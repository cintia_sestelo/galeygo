import unittest
from challenge5 import largest_palindrome


class TestHigherPalindrome(unittest.TestCase):
    def test_with_three_digits(self):
        self.assertEqual(largest_palindrome(), 906609)


unittest.main()
