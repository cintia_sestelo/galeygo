def max_value(sample):
    if not sample:
        return None

    max_number = sample[0]
    for number in sample:
        if number > max_number:
            max_number = number
    return max_number
