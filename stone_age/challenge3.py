def list_len(l):
    final_len = 0
    for element in l:
        final_len += 1
    return final_len

def list_generator(list1, list2):
    if list_len(list1) == list_len(list2):
        max_element = list_len(list1)
    else:
        raise ValueError('Lists have different lenght.')

    final_list = []
    for element in range(max_element):
        final_list.append(list1[element])
        final_list.append(list2[element])

    return final_list
